db.users.insertMany([
	{
		firstName: "Jane",
		lastName: "Doe",
		age: 67,
		contact: {
			phone: "5347786",
			email: "jane@doe.com"
		},
		courses: ["Phyton","React","PHP"],
		department: "HR"
	},
	{
		firstName: "Stephen",
		lastName: "Hawking",
		age: 76,
		contact: {
			phone: "5347786",
			email: "steph@hawking.com"
		},
		courses: ["React","Laravel","SASS"],
		department: "HR"
	},
	{
		firstName: "Neil",
		lastName: "Armstrong",
		age: 82,
		contact: {
			phone: "5347786",
			email: "neil@gmail.com"
		},
		courses: ["React","Laravel","SASS"],
		department: "HR"
	}
])

// 2.) 
db.users.find({
    $or:[
            {firstName: { $regex: "s", $options: "$i"}},
            {lastName: { $regex: "d", $options: "$i"}},
            
        ]
        },
            {
            firstName: 1,
            lastName: 1,
            _id: 0,
            
}) 

// 3.)
db.users.find({
	$and: [
		{
			age: { $gte: 70 }
		},
	]
})

// 4.)
db.users.find({
	$and: [
		{
			firstName: { $regex: "e", $options: "$i"},
		},
		{
			age: { $lte: 30 },
		}
	]
})